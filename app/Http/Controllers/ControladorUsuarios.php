<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class ControladorUsuarios extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.usuarios');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = new User();
        $usuario->nombre = $request->nombre;
        $usuario->email = $request->email;
        $usuario->correo = $request->correo;
        $usuario->tipo = $request->tipo;

        if($request->contraseña == "")
            $usuario->password = Hash::make($usuario->email);
        else
            $usuario->password = Hash::make($request->password);

        $usuario->save();

        Session(['exito' => "Se creo correctamente el usuario ".$usuario->email]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::find($id);
        
        return view('admin.usuarios_recibos', compact('usuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = User::find($id);
        $usuario->nombre = $request->nombre;
        $usuario->email = $request->email;
        $usuario->correo = $request->correo;
        $usuario->tipo = $request->tipo;

        if($request->password != "")
            $usuario->password = Hash::make($request->password);

        $usuario->save();

        Session(['exito' => "Se modifico correctamente el usuario ".$usuario->email]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::find($id);
        
        foreach($usuario->recibos as $recibo)
            $recibo->delete();

        $usuario->delete();

        Session(['exito' => "Se elimino correctamente el usuario ".$usuario->email]);
        return back();
    }
}
