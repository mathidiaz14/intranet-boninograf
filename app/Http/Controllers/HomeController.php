<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recibo;
use Auth;
use Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->tipo == "Administrador")
            return view('admin.usuarios');

        return view('home');
    }

    public function ver_usuario()
    {
        return view('usuario');
    }

    public function guardar_usuario(Request $request)
    {
        $usuario = Auth::user();

        $usuario->nombre = $request->nombre;
        $usuario->correo = $request->correo;

        if($request->contraseña_actual != "")
        {
            if(Hash::check($request->contraseña_actual, $usuario->password))
            {
                if($request->contraseña_nueva === $request->contraseña_repetida)   
                {
                    $usuario->password = Hash::make($request->contraseña_nueva);
                    
                }else
                {
                    Session(["error" => "Las contraseña nueva no coincide con la confirmaión."]);
                    return back();
                }
            }else
            {
                Session(["error" => "Las contraseñas actual no coincide con nuestro registro."]);
                return back();
            }
        }

        $usuario->save();
        
        Session(["exito" => "La datos de usuario se modificaron correctamente."]);
        return back();
    }

    public function descargar($id)
    {
        $recibo = Recibo::find($id);
        
        if($recibo != null) 
        {
            if((Auth::user()->tipo == "Administrador") or (Auth::user()->id == $recibo->user_id))
                return response()->file(storage_path('app/'.$recibo->archivo));
        }

        return back();
    }
}
