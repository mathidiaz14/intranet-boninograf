<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recibo;
use App\Models\User;
use Smalot\PdfParser\Parser;
use Storage;

class ControladorRecibo extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.recibos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        set_time_limit(240);

        $archivos = $request->file('archivos');

        foreach ($archivos as $archivo) 
        {
            //obtenemos el nombre del archivo
            $nombre = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 30).".pdf";

            //indicamos que queremos guardar un nuevo archivo en el disco local
            Storage::disk('local')->put($nombre,  \File::get($archivo));

            // Parse pdf file and build necessary objects.
            $parser = new Parser();
            $pdf    = $parser->parseFile(storage_path('app/'.$nombre));
             
            $text = $pdf->getText();
            
            foreach(User::all() as $user)
            {
                $encontro = strpos($text, $user->email);

                if($encontro == true)
                {
                    if ($user->recibos->where('fecha', $request->fecha)->first() == null)
                    {
                        $recibo = new Recibo();
                        $recibo->user_id = $user->id;
                        $recibo->fecha = $request->fecha;
                        $recibo->archivo = $nombre;
                        $recibo->save();
                    }
                    
                }
            }       
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $recibo = Recibo::find($id);

        $recibo->delete();

        Session(['exito' => "Se elimino correctamente el recibo"]);

        return back();
    }

    public function fecha($fecha)
    {
        $recibos = Recibo::where('fecha', $fecha)->get();

        return view('admin.recibos_fecha', compact('recibos'));
    }
}
