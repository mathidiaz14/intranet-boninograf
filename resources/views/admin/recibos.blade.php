@extends('layouts.app')

@section('content')

    <section class="p-t-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-data__tool">
                    	<div class="table-data__tool-left">
                    		<h3 class="title-5 m-b-35">Recibos por fecha</h3>
                    	</div>
                    </div>
                    @include('ayuda.alerta')
                    <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                                <tr>
                                	<th></th>
                                    <th>Fecha</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(App\Models\Recibo::all()->groupBy('fecha') as $recibo)
                                	<tr class="tr-shadow">
	                                    <td>
	                                    	@php $fecha = explode('-', $recibo->first()->fecha); @endphp
                                            {{$fecha[1]}}/{{$fecha[0]}}
	                                    </td>
                                        <td>
                                            <a href="{{url('admin/recibos/fecha', $recibo->first()->fecha)}}" class="btn btn-success">
                                                <i class="fa fa-eye"></i>
                                                Ver
                                            </a>
                                        </td>
	                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END DATA TABLE-->
@endsection