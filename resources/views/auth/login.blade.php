@extends('layouts.auth')

@section('content')
<div class="login-wrap">
    <div class="login-content">
        <div class="login-logo">
            <a href="#">
                <img src="{{asset('images/logo.png')}}" alt="CoolAdmin" width="50px" /> <b>Bonino Graf</b>
            </a>
        </div>
        @error('email')
            <span role="alert" style="  color: #721c24;
                                        background-color: #f8d7da;
                                        border-color: #f5c6cb;
                                        padding: .75rem 1.25rem;
                                        margin-bottom: 1rem;
                                        border: 1px solid transparent;
                                        border-radius: .25rem;
                                        width: 100%;">
                <strong>{{ $message }}</strong>
            </span>
            <br><br>
        @enderror
        @error('password')
            <span role="alert" style="  color: #721c24;
                                        background-color: #f8d7da;
                                        border-color: #f5c6cb;
                                        padding: .75rem 1.25rem;
                                        margin-bottom: 1rem;
                                        border: 1px solid transparent;
                                        border-radius: .25rem;
                                        width: 100%;">
                <strong>{{ $message }}</strong>
            </span>
            <br><br>
        @enderror
        <div class="login-form">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label>Cedula de identidad <small>(Sin puntos ni guiones)</small></label>
                    <input class="au-input au-input--full" type="text" name="email" placeholder="Ingrese aqui su CI">
                </div>
                <div class="form-group">
                    <label>Contraseña</label>
                    <input class="au-input au-input--full" type="password" name="password" placeholder="Ingrese aqui su contraseña">
                </div>
                <div class="login-checkbox">
                    <label>
                        <input type="checkbox" name="remember">Recuerdame
                    </label>
                </div>
                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">Iniciar Sesión</button>
            </form>
            
        </div>
    </div>
</div>

@endsection
