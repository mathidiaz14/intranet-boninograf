<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nombre' => 'Administrador',
            'email' => 'admin@admin.com',
            'correo' => 'admin@admin.com',
            'password' => Hash::make('123456'),
            'tipo' => 'Administrador',
        ]);
    }
}
