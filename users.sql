-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-09-2020 a las 22:57:02
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `intranet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'estandar',
  `correo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primer_inicio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'si',
  `cambio_contraseña` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombre`, `email`, `email_verified_at`, `password`, `tipo`, `correo`, `primer_inicio`, `cambio_contraseña`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'admin@admin.com', NULL, '$2y$10$VFHtMMlkZf1UHCs39D19DuQ50GmYPD0s1OrMV8N8qC5sjhN7CTXB.', 'Administrador', NULL, 'si', 'no', NULL, NULL, NULL),
(3, 'JOSE EDUARDO SOUTO QUEVEDO', '16733071', NULL, '$2y$10$4KTFBQIbakxYOuMl0TEMYO/EuQd7Ca3nJFhui7KdfYkciKxhl.o4W', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:32:18', '2020-09-14 19:32:18'),
(4, 'ADRIANA RODRIGUEZ OLIVERA', '17736347', NULL, '$2y$10$Hp4p00.1PFJ6jg9PW4YhaeHnvaIdr85xs2Xn3rp904NcBj6HIE882', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:32:36', '2020-09-14 19:32:36'),
(5, 'JOSE CARLOS BRAGA', '17968962', NULL, '$2y$10$vwMyzkVCianJUzNcl5QpMO3m0T7Fisokij0q/2/m1sHOAye0.olLW', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:32:49', '2020-09-14 19:32:49'),
(6, 'GERARDO ARIEL MIGUEL PALACIO', '18599689', NULL, '$2y$10$Os60ls.fM7DLUvvZ6YjnGO2lZfC7VnhL7apjfcpUnzaJGl53MlVJ2', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:33:08', '2020-09-14 19:33:08'),
(7, 'DARIO ALEXANDER FERRE JUAREZ', '19842859', NULL, '$2y$10$ucl2SkpVdp2U8JTevPu0QOBQ.fh8eQb.d8dIl0jH/BX4UXJozoKFu', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:33:22', '2020-09-14 19:33:22'),
(8, 'JOSE MARIA BERON', '25035549', NULL, '$2y$10$z4/Orh1gbibooLgv1K3GRuvemHMgIBMxAKEfDVhiZ2FXKuocNzJ0W', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:33:34', '2020-09-14 19:33:34'),
(9, 'JUAN ANTONIO MENDOZA TARAMASCO', '25707520', NULL, '$2y$10$BEMB7enLwJhaMZh9dTjWNe2xQPE6kKL1rScJdTx9Yp5EfHL6EW.Fe', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:34:08', '2020-09-14 19:34:08'),
(10, 'FERNANDO JAVIER GUTIERREZ CABALLERO', '26382668', NULL, '$2y$10$0ZAmsHbBY4OFYOgUhR4ubuQe1bFpJ98cUnuw5qzxh705JE3e8MYkO', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:34:39', '2020-09-14 19:34:39'),
(11, 'ROSARIO YANET ALVAREZ CORUJO', '27033838', NULL, '$2y$10$vNb6nyguuH1bWGJ/.zOeueD0h7tR4E.R2GOWAcSZAVYs7ShLZ1mrm', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:34:53', '2020-09-14 19:34:53'),
(12, 'MARDOQUIO RIVERO VEGA', '30495297', NULL, '$2y$10$y29rCiZFoAVVZ.jr5ChPbe0bdkjNa8sdEW.amsk8AtU6zaRwckvTe', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:35:08', '2020-09-14 19:35:08'),
(13, 'HECTOR GERMAN CAYETANO RODRIGUEZ', '30832869', NULL, '$2y$10$Fod39L6fMsBwNXW6KaY57e9BYKCVnEMSYrKtoS31nS/H1R34vChvG', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:35:31', '2020-09-14 19:35:31'),
(14, 'JORGE ARIEL LOPEZ CORREA', '31511143', NULL, '$2y$10$Scijvk9rUh7ifYuPzN5kDOgdJy4LMGOvCYPEy6dGjbND.5QM9yHde', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:35:42', '2020-09-14 19:35:42'),
(15, 'PEDRO MANUEL OCAMPO SOSA', '32847886', NULL, '$2y$10$maQMgxnu.ZuyRCeBQCaWkebouMxcE7iZVkpqclvP9jph79j5lU4h6', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:35:54', '2020-09-14 19:35:54'),
(16, 'CARLOS JAVIER RODRIGUEZ SILVA', '34984040', NULL, '$2y$10$XDxQ/RO/ScIO8FkOALqWw.Kkq/yYHLs1MPBrBmHXYhdx.w3Y0FGOW', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:36:07', '2020-09-14 19:36:07'),
(17, 'LUIS MARCELO BERNIZ', '35102112', NULL, '$2y$10$.4g9JggcofIwx0pJRTkJ/umoN/FdQBdlN1uJ.gYRDTSucH1jiQHgG', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:36:20', '2020-09-14 19:36:20'),
(18, 'JUAN JOSE CASTRO SOSA', '35973210', NULL, '$2y$10$RrLPJpy1WLd1Vc4NL0oE5uuCnG5PTh4/bcgIveWjra2Mt3Gsm1zCK', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:36:31', '2020-09-14 19:36:31'),
(19, 'LUIS ALBERTO DE LOS SANTOS', '36272627', NULL, '$2y$10$OOEIjp/fh3tgyfwed.sFiOSc3mBg4ca18Lmf3N/V2/qxRw9JYmww2', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:36:46', '2020-09-14 19:36:46'),
(20, 'RICHARD W. GUINTEMBERG ALVAREZ', '38891902', NULL, '$2y$10$dQFFIElwM0U/SxZQd8mj2OixhpPUVG.m4FKBgxeO7KdcE278QSc4y', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:37:01', '2020-09-14 19:37:01'),
(21, 'RUBEN EDUARDO MILLACET TRAVIESO', '39371082', NULL, '$2y$10$rDPApa.iVuxl8sISC.VlaO9M12moxOeGqtDqqD3PjNAQAHcaLYipq', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:39:28', '2020-09-14 19:39:28'),
(22, 'MARIA ELIZABETH RIVERO FLORES', '40588036', NULL, '$2y$10$q0ioBQs4EVbhKnkfgniGf.056SggP5yUMDynu/e6Cd/8XwYsNgaSW', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:39:40', '2020-09-14 19:39:40'),
(23, 'CESAR MARIO SANCHEZ', '41550622', NULL, '$2y$10$i9f6K.Ae.HxpQ/I1n2SZVu1/53WJEq9wfy73iB/MK/ooRfJB.8FnG', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:39:53', '2020-09-14 19:39:53'),
(24, 'JOSE GABRIEL ORONA PACCIOº', '42627088', NULL, '$2y$10$ykGXtvdxlFCwL87gJBwav.ukzdyTfZ.Wi7ClUQGk1r7OIej3kN7iW', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:40:07', '2020-09-14 19:40:07'),
(25, 'SILVIA LOURDES FERNANDEZ PARANDELLI', '44310607', NULL, '$2y$10$LFw2GF1PgcnZD1MapZDIYuKE8jIGSTc6F82TVnkN3bvKDzT4feWTm', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:40:51', '2020-09-14 19:40:51'),
(26, 'BERNARDO FABRICIO LEON FURCO', '44379639', NULL, '$2y$10$Ro1KVZ2n33JBOAOCIoQIZukQqImh6DIhejwripI8k2cs4qHy.yX5i', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:42:04', '2020-09-14 19:42:04'),
(27, 'LEONARDO M. MARISTAN DE LOS SANTOS', '44575160', NULL, '$2y$10$rM7gq9nhkM2qsBgcmef5NusLyE4FzEPHRRkh3ByK3NsHVCjqRVUYe', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:42:20', '2020-09-14 19:42:20'),
(28, 'CRISTIAN BERNARDO MUNIZ PERAZZA', '44672706', NULL, '$2y$10$4iIVIXfkZYlQTyYVBlNLsObtOIZ7D9e2zGIBLvBcmPnteEas6FwLy', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:42:46', '2020-09-14 19:42:46'),
(29, 'MICHAEL A. MARISTAN DE LOS SANTOS', '44860210', NULL, '$2y$10$mzoJZ1sVfLpSXhzMDw/8EeseciZkB2y3mT2V46YAkqNXePPFBpa66', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:43:46', '2020-09-14 19:43:46'),
(30, 'ANTONIO MIGUEL LOPEZ MELGAREJO', '49508611', NULL, '$2y$10$vja5R/r.5oVEqYPaL8rN.usiS9ZU9KQ8wJQbIiKVdT6005pw5VIOu', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:44:02', '2020-09-14 19:44:02'),
(31, 'WALTER GUSTAVO COSTA DA ROSA', '49625275', NULL, '$2y$10$mA03M.sn0RRE14MR/CvxpO3zJKQoq1ymh2D0FTp4qdL4m2wVA5RWa', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:44:16', '2020-09-14 19:44:16'),
(32, 'MAURO PEREZ BARRIOS', '52619611', NULL, '$2y$10$QToLDrloLMi4Iq6KGdtnmuMwVL8fkX6NGbc7Xy.3lCAPbAV3c5Oni', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:44:35', '2020-09-14 19:44:35'),
(33, 'JOSE MAURICIO GUERRERO PARANDELLI', '53466332', NULL, '$2y$10$/FZnuV8TQrEvvbztcr398e5PSDeXk9KOFED/abzWS9sW5GAjX7S7.', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:44:54', '2020-09-14 19:44:54'),
(35, 'BERNARDO JAVIER LEON GALEANO', '53752696', NULL, '$2y$10$oUBiHkFKpcYGIGlgAf5I2uQJRdfDHsn6DYsq8U4wvJYl7wVmlHDv6', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:45:38', '2020-09-14 19:45:38'),
(36, 'LUCIA RODRIGUEZ PALLAS', '59887013', NULL, '$2y$10$NR0tyMp1crgr1O.5nn5cbup6uVqBwVYxyort04x4hvr6lU0lnWTo6', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:46:37', '2020-09-14 19:46:37'),
(37, 'WILME YASEL TEJEDA VILLAR', '60966804', NULL, '$2y$10$gwGR.623MoRiV/M6EJyGMec1BX03kwgTCXxpzXql1Atj1PRBkoIwS', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:47:02', '2020-09-14 19:47:02'),
(38, 'CARLOS FEDERICO PEIGONET CAPUCHO', '60966805', NULL, '$2y$10$ksgW2MlQcSFgQ.2.qHxh.O6jczvAz8BEy83ptSsKrMR3LtoirNml2', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:47:19', '2020-09-14 19:47:19'),
(40, 'REINALDO RAMON PEÑA CASTILLO', '63295626', NULL, '$2y$10$rkdQsXPI65apqC0x3WRt/egHRlHeBFKE.cN6PujYeTMnkda5GbP3q', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:48:06', '2020-09-14 19:48:06'),
(41, 'LUIS JACINTO MORENO RONDON', '63362821', NULL, '$2y$10$lcbGms23LxFkz8DW.8jGr.PTMGjhyYJwDLqzR0hQXEjBmWP2OlC92', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:48:30', '2020-09-14 19:48:30'),
(42, 'CARLOS FERNANDO BONINO GRAF', '30423066', NULL, '$2y$10$NAzCCWKKJi7roQID5jZNp.1cmvTinYRhG1ZX.1FLvgIJM5mIYN3Nu', 'Estandar', NULL, 'si', 'no', NULL, '2020-09-14 19:48:55', '2020-09-14 19:48:55');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
