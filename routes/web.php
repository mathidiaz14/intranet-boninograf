<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ControladorRecibo;
use App\Http\Controllers\ControladorNoticias;
use App\Http\Controllers\ControladorUsuarios;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class ,'index'])->middleware('auth');

Route::get('home', [HomeController::class ,'index'])->middleware('auth');
Route::get('usuario', [HomeController::class ,'ver_usuario'])->middleware('auth');
Route::post('usuario', [HomeController::class ,'guardar_usuario'])->middleware('auth');
Route::get('descargar/{ID}', [HomeController::class ,'descargar'])->middleware('auth');

Route::resource('admin/recibos', ControladorRecibo::class)->middleware('auth.admin');
Route::get('admin/recibos/fecha/{fecha}', [ControladorRecibo::class ,'fecha'])->middleware('auth.admin');
Route::resource('admin/noticias',ControladorNoticias::class)->middleware('auth.admin');
Route::resource('admin/usuarios',ControladorUsuarios::class)->middleware('auth.admin');